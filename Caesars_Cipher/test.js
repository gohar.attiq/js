describe("Cipher Test Cases", function() {

    it("My Name is Gohar ciphered with rot 3 is Pb Qdph lv Jrkdu", function() {
      assert.equal(caesarsCipher(3,"My Name is Gohar"), "Pb Qdph lv Jrkdu");
    });
  
    it("My Name is Gohar ciphered with rot 8 is Ug Vium qa Owpiz", function() {
      assert.equal(caesarsCipher(8,"My Name is Gohar"), "Ug Vium qa Owpiz");
    });
  
  });
  