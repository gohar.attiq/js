const onClick = () => {
    const rot = document.getElementById('rot').value;
    const text = document.getElementById('text').value;

    if(!text){
        alert("Please Enter In TextArea!!");
        return;
    }

    cipherText = caesarsCipher(Number(rot), text);
    renderCipherText(cipherText);
}

const caesarsCipher = (rot, text) => {
    let cipherText = "";

    for (let i = 0; i < text.length; i++) {
        if(isUpperCase(text[i])){
            let char = String.fromCharCode((text[i].charCodeAt(0) + rot-65) % 26 + 65);
            cipherText += char;
        }else if (isLowerCase(text[i])) {
            let char = String.fromCharCode((text[i].charCodeAt(0) + rot-97) % 26 + 97);
            cipherText += char;
        } else {
            cipherText += text[i];
        }
    }

    return cipherText;
}

const renderCipherText = (text) => {
    if (!alreadyAreaField) {
        const div = document.createElement('div');
        const span = document.createElement('span');
        const textarea = document.createElement('textarea');

        div.className += 'input-group w-50 mt-4';
        span.className += 'input-group-text';
        span.innerText = 'Cipher Text';
        textarea.id = 'cipherText';
        textarea.className += 'form-control';
        textarea.rows  = 10;

        div.appendChild(span);
        div.appendChild(textarea);
        document.getElementById('root').appendChild(div);
 
        alreadyAreaField = true;
    }
    const cipherText = document.getElementById('cipherText');
    cipherText.textContent = text;
}

let alreadyAreaField = false;

const isUpperCase = (str) => !/[a-z]/.test(str) && /[A-Z]/.test(str);
const isLowerCase = (str) => /[a-z]/.test(str) && !/[A-Z]/.test(str);

const button = document.getElementById('btn');
button.addEventListener('click', onClick);
